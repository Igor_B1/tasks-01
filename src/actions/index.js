import axios from 'axios';
import {
    GET_ACTIVE_TASKS,
    CLEAR_TASKS,
    GET_TASK,
    EDIT_TASK,
    IS_EDITING
} from "./types";
export const URL = 'http://localhost:3000';

// tasks

const getTasks = () => dispatch => {
    axios.get(`${URL}/tasks`)
        .then(res => {
            dispatch({
                type: GET_ACTIVE_TASKS,
                payload: res.data
            })
        })
        .catch(err => console.error(err));
};

const clearTasks = () => ({ type: CLEAR_TASKS, payload: [] });


// single task

const getTask = id => dispatch => {
    axios.get(`${URL}/tasks/${id}`)
        .then(res => {
            dispatch({
                type: GET_TASK,
                payload: res.data
            })
        })
        .catch(err => console.error(err));
};


const editTask = ( id, updates ) => ( dispatch ) => {
    axios.put(`${URL}/tasks/${id}`, updates)
        .then(res => {
            dispatch({
                type: EDIT_TASK,
                payload: updates
            })
        })
        .catch(err => console.error(err));
};


const toEditingMode = () => ({ type: IS_EDITING });


export {
    getTasks,
    clearTasks,
    getTask,
    toEditingMode,
    editTask
};
