import React from 'react';

const TaskCard = ({ task, isEditing, taskEdit, toEditMode }) => {
    const renderDescription = ({ description }) => (
        <div onClick={ toEditMode }>
            {
                description ?
                    <span>{description}</span> :
                    <span>No description... Add description if needed</span>
            }
        </div>
    );

    const renderInput = ({ description }) => (
        <input
            type="text"
            className="input"
            defaultValue={description}
            onBlur={ taskEdit }
            autoFocus
        />
    );

    return (
        <div className="task__card">
            <div className="task__card--header">
                <span>
                    { task.name }
                </span>
                <span>
                    Progress: { task.physical_progress } %
                </span>
            </div>

            <hr className="task__card--hr"/>
            <div className="task__card--description">
                {
                    isEditing ?
                        renderInput(task) :
                        renderDescription(task)
                }
            </div>
        </div>
    );
};

export default TaskCard;