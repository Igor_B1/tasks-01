import React from 'react';
import { Switch, Route } from "react-router-dom";
import TasksTable from './TasksTable';
import Task from './Task';

const App = () => (
    <div className="app">
        <Switch>
            <Route path='/tasks/:id' component={Task}/>
            <Route path='/' component={TasksTable} exact/>
        </Switch>
    </div>
);

export default App;
