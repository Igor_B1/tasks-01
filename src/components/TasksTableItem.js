import React from 'react';
import {Link} from "react-router-dom";

const TasksTableItem = ({
   id,
   name,
   actual_effort,
   estimated_effort,
   tags,
   due_date,
   is_high_priority
}) => (
    <tbody>
        <tr className="tasks__table--row">
            <td>
                {
                    is_high_priority ?
                        <Link to={`/tasks/${id}`} className="task task__priority">{name}</Link> :
                        <Link to={`/tasks/${id}`} className="task">{name}</Link>
                }
            </td>
            <td>{actual_effort}</td>
            <td>{estimated_effort}</td>
            <td>{due_date}</td>
            <td>{tags ? tags.toString() : null}</td>
        </tr>
    </tbody>
);

export default TasksTableItem;