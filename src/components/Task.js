import React from 'react';
import { getTask, toEditingMode, editTask } from "../actions";
import { connect } from "react-redux";
import TaskCard from "./TaskCard";

class Task extends React.Component {
    componentDidMount() {
        const { id } = this.props.match.params;

        this.props.getTask(id);
    }

    edit = (description) => {
        const { task } = this.props.task;
        const editedTask = { ...task, description };

        this.toEditMode();

        if(task.description !== editedTask.description) {
            this.props.editTask(task.id, editedTask);
        }
    };

    taskEdit = (e) => this.edit(e.target.value);

    toEditMode = () => this.props.toEditingMode();

    render() {
        const { task, isEditing } = this.props.task;

        return (
            <TaskCard
                task={task}
                isEditing={isEditing}
                taskEdit={this.taskEdit}
                toEditMode={this.toEditMode}
            />
        );
    }
}

export default connect(
    ({ task }) => ({ task }),
    { getTask, editTask, toEditingMode }
)(Task);