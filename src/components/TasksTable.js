import React from 'react';
import TasksTableItem from './TasksTableItem';
import { getTasks, clearTasks } from "../actions";
import { connect } from "react-redux";

class TasksTable extends React.Component {
    componentDidMount() {
        this.props.getTasks();
    }

    componentWillUnmount() {
        this.props.clearTasks();
    }

    render() {
        const { tasks } = this.props;
        return (
            <table className="tasks__table">
                <thead>
                    <tr>
                        <th>Task</th>
                        <th>Actual Effort</th>
                        <th>Estimated Effort</th>
                        <th>Due date</th>
                        <th>Tags</th>
                    </tr>
                </thead>
                { tasks.map(task => <TasksTableItem key={task.id} {...task}/>) }
            </table>
        )
    }
}

export default connect(
    ({ tasks }) => ({ tasks }),
    { getTasks, clearTasks }
)(TasksTable);