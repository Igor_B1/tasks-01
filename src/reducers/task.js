import { GET_TASK, EDIT_TASK, IS_EDITING } from "../actions/types";

const initialState = {
    isEditing: false,
    task: {}
};

export default ( state = initialState, action ) => {
    switch (action.type) {
        case GET_TASK:
            return { ...state, task: action.payload };
        case EDIT_TASK:
            return { ...state, task: action.payload };
        case IS_EDITING:
            return { ...state, isEditing: !state.isEditing };
        default:
            return state;
    }
}
