import { CLEAR_TASKS, GET_ACTIVE_TASKS } from "../actions/types";


export default (state = [], action) => {
    switch (action.type) {
        case GET_ACTIVE_TASKS:
            const activeTasks = action.payload.filter(item => item.obj_status === 'active');
            return [...state, ...activeTasks];
        case CLEAR_TASKS:
            return action.payload;
        default:
            return state;
    }
}