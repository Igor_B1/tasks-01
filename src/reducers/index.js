import { combineReducers } from 'redux';
import tasks from './tasks';
import task from './task';

export default combineReducers({
    tasks,
    task
});