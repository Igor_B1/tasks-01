import singleTaskReducer from '../../reducers/task';
import { GET_TASK, IS_EDITING } from "../../actions/types";


describe('task reducer', () => {
    const initialState = {
        isEditing: false,
        task: {}
    };

    test('should return initial state', () => {
        const state = singleTaskReducer(undefined, {type: '@@INIT'});
        expect(state).toEqual({
            isEditing: false,
            task: {}
        })
    });

    test('should turn on isEditing mode', () => {
        const state = singleTaskReducer(initialState, {type: IS_EDITING});
        expect(state).toEqual({
            isEditing: true,
            task: {}
        })
    });

    test('should get task', () => {
        const task = {
            id: 1,
            name: 'name',
            description: 'description'
        };

        const state = singleTaskReducer(initialState, {type: GET_TASK, payload: task});
        expect(state).toEqual({
            isEditing: false,
            task
        })
    });
});
