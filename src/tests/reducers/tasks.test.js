import tasksReducer from '../../reducers/tasks';
import {CLEAR_TASKS, GET_ACTIVE_TASKS} from "../../actions/types";




describe('tasks reducer', () => {
    test('should return initial state', () => {
        const state = tasksReducer(undefined, {type: '@@INIT'});
        expect(state).toEqual([])
    });

    test('should clear tasks list', () => {
        const state = tasksReducer([], {type: CLEAR_TASKS, payload: []});
        expect(state).toEqual([]);
    });

    test('should get active tasks', () => {
        const allTasks = [
            {
                id: 1,
                name: 'task 1',
                obj_status: 'active'
            },
            {
                id: 2,
                name: 'task 2',
                obj_status: 'done'
            }
        ];

        const activeTasks = [
            {
                id: 1,
                name: 'task 1',
                obj_status: 'active'
            }
        ];

        const state = tasksReducer([], {type: GET_ACTIVE_TASKS, payload: allTasks});
        expect(state).toEqual(activeTasks);
    })
});