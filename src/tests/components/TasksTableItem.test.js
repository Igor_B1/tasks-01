import React from 'react';
import { shallow } from 'enzyme';
import TasksTableItem from '../../components/TasksTableItem';
import {Link} from "react-router-dom";

describe('task table item component', () => {
    let wrapped;
    beforeEach(() => {
        wrapped = shallow(<TasksTableItem/>);
    });

    test('should contain 5 table data cells', () => {
        expect(wrapped.find('td').length).toBe(5);
    });

    test('should contain 1 link', () => {
        expect(wrapped.find(Link).length).toBe(1);
    })
});