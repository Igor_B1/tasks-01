import {
    clearTasks,
    toEditingMode
} from "../../actions";
import { CLEAR_TASKS, IS_EDITING } from "../../actions/types";


test('should clear tasks', () => {
    const action = clearTasks();
    expect(action).toEqual({
        type: CLEAR_TASKS,
        payload: []
    })
});


test('should turn on editing mode', () => {
    const action = toEditingMode();
    expect(action).toEqual({
        type: IS_EDITING
    })
});
