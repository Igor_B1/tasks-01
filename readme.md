**What were using**

* React 16
* Redux 
* React-Router 4
* HTML5 + SCSS
* axios
* Webpack 4
* Jest, Enzyme.


**To run**

Fork and clone the project:

``git clone git@bitbucket.org:Igor_B1/tasks-01.git``

Then install the dependencies:

``npm install``
 
Build the project:

``npm run build``

In order to run the project open the first terminal and type

``npm run dev``
 
command, also in the second terminal type 

``npm run serve``

To run tests:

``npm run test``
